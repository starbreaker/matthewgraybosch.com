# makefile for matthewgraybosch.com

# Copyright 2021 Matthew Graybosch <contact@matthewgraybosch.com>

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:

# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

HTM!=find site/ -name "*.htm"
PNG!=ls site/images/*.png
JPG!=ls site/images/*.jpg
HTML=${HTM:.htm=.html}
JPG_WEBP=${JPG:.jpg=.webp}
PNG_WEBP=${PNG:.png=.webp}

.SUFFIXES: .htm .html .jpg .png .webp

.DEFAULT: build

.htm.html:
	sed -f ./en_utf8_quotes.sed "$<" | hxincl -f | hxtoc -f -l 3 | hxnormalize > "$@"

.jpg.webp:
	convert "$<" -quality 50 "$@"
	convert "$<" -resize 255x "${<:S/images/images\/thumb/}"
	convert "$<" -resize 255x -quality 50 "${@:S/images/images\/thumb/}"

.png.webp:
	convert "$<" -quality 50 "$@"
	convert "$<" -resize 255x "${<:S/images/images\/thumb/}"
	convert "$<" -resize 255x -quality 50 "${@:S/images/images\/thumb/}"

build: $(HTML) $(JPG_WEBP) $(PNG_WEBP) site/index.htm

dotfiles:
	cp ~/.kshrc site/dotfiles/kshrc.txt
	cp ~/.Xresources site/dotfiles/Xresources.txt
	cp ~/.xsession site/dotfiles/xsession.txt
	cp ~/.cwmrc site/dotfiles/cwmrc.txt
	cp ~/.emacs site/dotfiles/emacs.txt
	cp makefile site/dotfiles/makefile.txt
	cp site/.htaccess site/dotfiles/htaccess.txt

manuals:
	cp manuals/rockout.html site/unix/rockout/rockout-manual.html
	cp manuals/rockout.pdf site/unix/rockout/rockout-manual.pdf

serve:
	python3 -m http.server --directory site/

commit:
	git add .
	git commit
	git push

install: commit build
	rsync --rsh="ssh ${SSH_OPTS}" \
		  --delete-delay \
		  --exclude-from='./exclude.txt' \
		  -acvz \
		  site/ ${REMOTE_HOST}:${REMOTE_PATH}

clean:
	rm $(HTML) $(JPG_WEBP) $(PNG_WEBP) site/images/thumb/*

test:
	@echo ${SSH_OPTS}
	@echo ${REMOTE_HOST}
	@echo ${REMOTE_PATH}
