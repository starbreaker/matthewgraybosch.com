<!doctype html>
<html lang="en">
  <head>
	<title>rockout: a shell script &middot; Matthew Graybosch</title>
	<meta property="og:title" content="rockout: a shell script by Matthew Graybosch" />
	<meta name="description" content="rockout - generate and listen to playlists in your terminal" />
	<meta property="og:description" content="rockout - generate and listen to playlists in your terminal" />
	<meta property="twitter:description" content="rockout - generate and listen to playlists in your terminal" />
	<link rel="canonical" href="https://matthewgraybosch.com/" />

	<!--begin-include "/home/starbreaker/internet/matthewgraybosch.com/includes/meta.html" -->
	<!--end-include -->
	
  </head>
  <body>

	<!--begin-include "/home/starbreaker/internet/matthewgraybosch.com/includes/header.html" -->
	<!--end-include -->

	<!--begin-include "/home/starbreaker/internet/matthewgraybosch.com/includes/nav-unix.html" -->
	<!--end-include -->
	
	<article id="content">
	  <h2>rockout - generate and listen to playlists in your terminal</h2>
	  <p>
		<small><em>Published on <time timestamp="2021-04-14">14 April 2021</time></em></small>
	  </p>
	  
	  <!--begin-toc-->
      <!--end-toc-->

	  <section>
		<h3>Introduction</h3>
		<p><strong><code>rockout</code></strong> is a little POSIX shell script that generates a plain-text database of your music collection, lets you query it using the same extended regular expressions supported by <code>grep -E</code>, and pipes your search results to <a href="https://mpv.io/" target="_blank">mpv</a> with the ability to loop and shuffle.</p>
		<p>It’s freely available under the 2-clause BSD license, and was developed and tested on OpenBSD. It should also work on your favorite GNU/Linux and GNU/systemd distributions, but you’ll have to install it yourself.</p>
	  </section>
	  <section>
		<h3>Download</h3>
		<p>Download <a href="https://codeberg.org/starbreaker/rockout/releases/tag/v0.8.1" target="_blank"><code>rockout</code> v0.8.1</a> from Codeberg.</p>
		<p>Get <a href="https://codeberg.org/starbreaker/rockout/" target="_blank"><code>rockout</code> source code</a> from Codeberg.</p>
	  </section>
	  <section>
		<h3>Installation</h3>
		<p><code>rockout</code> comes with a makefile for easy installation from the terminal. If you’ve never installed software using a makefile before, I’ve provided instructions below.</p>
		<pre><code>$ curl --output rockout.tar.gz https://codeberg.org/starbreaker/rockout/archive/v0.8.1.tar.gz
$ tar xzvf rockout.tar.gz 
$ cd rockout
$ ls -hal
$ sudo make install
$ man rockout</code></pre>
		<p>By default, the makefile will attempt to put <code>rockout</code> in <code>/usr/local/bin</code> and its manual page in <code>/usr/local/man/man1</code>. If you’d prefer the base directory to be <code>/usr</code>, run <code>sudo make PREFIX=/usr install</code>.</p>
		<p>If you’re using OpenBSD, like me, you should replace <code>curl --output</code> with <code>ftp -o</code> and use <code>doas</code> instead of <code>sudo</code> (but you probably already knew that).</p>
	  </section>
	  <section>
		<h3>Installation Without Admin Privileges</h3>
		<p>If you’re reluctant or unable to install <code>rockout</code> system-wide, here are additional instructions. Again, please type these into your terminal once you’re in the <code>rockout</code> directory.</p>
		<pre><code>$ mkdir -p ~/bin
$ cp rockout ~/bin/rockout
$ chmod +x ~/bin/rockout</code></pre>
		<p>You probably won’t be able to use <code>man rockout</code> without a system-wide installation, but the archive comes with <a href="rockout-manual.html">HTML</a> and <a href="rockout-manual.pdf">PDF</a> versions of the manual page. They’re also available on this website.</p>
	  </section>
	  <section>
		<h3>Creating Your Library</h3>
		<p>To start using <code>rockout</code>, first use this command:</p>
		<pre><code>MUSIC_DIR=$HOME/Music rockout -l</code></pre>
		<p>Assuming your music collection lives in <code>~/Music</code> (the default location for most desktop-oriented distros), rockout will create a plain text database in <code>.local/rockout/library.m3u</code>.</p>
	  </section>
	  <section>
		<h3>Playing Music</h3>
		<p>Once you have rockout and mpv installed and have created your library, you’re all set. Try typing <code>rockout -rs &lt;band&gt;</code> to play your favorite artist on repeat/shuffle. Once the music starts, you can control playback using <code>mpv</code>’s key bindings. If you’re familiar with <a href="https://www.cyberciti.biz/faq/grep-regular-expressions/" target="_blank">extended regular expressions in grep</a> you can use them to generate complex playlists.</p>
	  </section>
	  <section>
		<h3>Saving Playlists</h3>
		<p>If you want to save a playlist, you can use the <code>-q</code> option and shell redirection. If you have a playlists directory in <code>~/Music</code>, you can even update your library again using the <code>-l</code> option and find your saved playlists using <code>rockout</code>.</p>
		<p>Suppose, for example, I wanted a 1980s playlist. This command will create the playlist, update the database to include it, and show its presence.</p>
<pre><code>rockout -q '/198[0-9]' > music/playlists/1980s.m3u; rockout -l; rockout -q 1980s</code></pre>
	  </section>
	  <section>
		<h3>Exporting Your Library</h3>
		<p>rockout uses a standard M3U playlist with absolute paths as its database. You can load <code>~/.local/rockout/library.m3u</code> into any player that handles M3U playlists, such as VLC, and it will work.</p>
	  </section>
	  <section>
		<h3>Dependencies</h3>
		<ul>
          <li>a Unix-like operating system with /bin/sh and the standard utilities</li>
          <li>mpv</li>
          <li>a reasonably well-organized music collection: your music directory should have a layout similar to <strong>artist/year/album/disc.track.title</strong> for best results</li>
		</ul>
	  </section>
	  <section>
		<h3>FAQ</h3>
		<dl>
		  <dt>Q: Why did you write this?</dt>
		  <dd>A: I wrote <code>rockout</code> for my own use. You’re welcome to use it, too.</dd>
		  <dt>Q: Aren’t there lots of music players already?</dt>
		  <dd>A: There are plenty of music players, even on OpenBSD, and many of them even work in a terminal, like <code>cmus</code> and <code>mpd</code> with <code>mpc</code> or <code>ncmpcpp</code>. However, <abbr title="Free and Open Source Software">FOSS</abbr> isn’t Burger King&trade;. If you want it done your way, you’ve got to do it yourself. So I did it <em>my way</em>.</dd>
		  <dt>Q: Why use regular expressions for search?</dt>
		  <dd>A: Because grep is fast. My own collection has over 18,000 tracks/playlists and weighs about 1.8MB. Querying the database takes less than a second; grep spends more time dumping the results to my terminal than it does <em>getting</em> them. Also, I live in text editors. The more skill I have with regex, the more effectively I can use my editor.</dd>
		  <dt>Q: Do you parse HTML with regex?</dt>
		  <dd>A: Only when I need to summon eldritch horrors from beyond normal spacetime and I’ve misplaced my copy of the <em>Pnakotic Manuscript</em>.</dd>
		  <dt>Q: Are you insane?</dt>
		  <dd>A: Probably, but after twenty years in this trade I’m still not sure if that’s a prerequisite for a career in tech or an occupational hazard.</dd>
		  <dt>Q: Can I use or modify this?</dt>
		  <dd>A: <code>rockout</code> is available under the 2-clause BSD license. Honor the terms of the license and we’re cool.</dd>
		  <dt>Q: Can I package this?</dt>
		  <dd>A: Go right ahead. Please email me if you do so, so I can give you credit here.</dd>
		  <dt>Q: Can you please add $FEATURE?</dt>
		  <dd>A: Thanks for saying ‘please’, but no. I wrote this for my own use first and foremost.</dd>
		  <dt>Q: Can you please include my modification?</dt>
		  <dd>A: Please open a pull request. If your mod looks like something I might want to use, I’ll merge it. Otherwise, you’re welcome to maintain your own fork.</dd>
		  <dt>Q: <code>rockout</code> doesn’t work for me. Can you help?</dt>
		  <dd>A: I’m sorry to hear that. Please file an issue and include the command you used along with the resulting error message. If I can reproduce it locally, I’ll try to fix it.</dd>
		  <dt>Q: How do I get <code>rockout</code> to display album art?</dt>
		  <dd>A: Use the <code>-d</code> switch, which I introduced in v0.6.</dd>
		  <dt>Q: Will you change the license to $LICENSE?</dt>
		  <dd>A: No.</dd>
		  <dt>Q: But $INFLUENCER says BSD is a “cuck license”.</dt>
		  <dd>A: I don’t lend credence to the opinions of channers and alt-reich cultists who use “cuck” because people won’t let them call things they dislike “gay” any longer. I don’t think you should let these people live in your head rent-free either, but that’s <em>your</em> business.</dd>
		  <dt>Q: Are you a <abbr title="social justice warrior">SJW</abbr>?</dt>
		  <dd>A: I don’t think so, but I’m also not interested in deliberately hurting people unless they’ve picked a fight with me.</dd>
		  <dt>Q: When will the Windows version come out?</dt>
		  <dd>A: When <em>you</em> implement it.</dd>
		  <dt>Q: Why do you have such a bad attitude?</dt>
		  <dd>A: Go and stare at your bathroom mirror until you realize that you’re part of the problem.</dd>
		</dl>
	  </section>
	</article>
	
	<!--begin-include "/home/starbreaker/internet/matthewgraybosch.com/includes/footer-2021.html" -->
	<!--end-include -->
	
  </body>
</html>
