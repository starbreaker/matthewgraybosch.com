# .cwmrc
# Copyright 2021 Matthew Graybosch <contact@matthewgraybosch.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# automatically add new windows to the current group
sticky yes

# snap windows to edges at 4 pixels distance
snapdist 5

# leave a gap so we can still use the rat when windows are maximized
gap 10 10 10 10

# set border
borderwidth 3

# border color settings
color activeborder "#d8dee9"
color inactiveborder "#a7adba"
color urgencyborder "#ec5f67"
color groupborder "#99c794"
color ungroupborder "#6699cc"

# menu color settings
color menubg "#1b2b34"
color menufg "#c0c5ce"
color font "#c0c5ce"
color selfont "#1b2b34"

# set Xft font
fontname "Noto Mono-10:dpi=200:antialias=true"

# these commands will appear in the command menu
command "emacs" "emacsclient -c -a ''"
command top "xterm -e top -U starbreaker"
command "mastodon" "xterm -geometry 80x35 -title mastodon -e tootstream"
command "irc (freenode.net)" "xterm -geometry 80x35 -title freenode.net -e catgirl freenode"
command "irc (tilde.chat)" "xterm -geometry 80x35 -title tilde.chat -e catgirl tildechat"
command screenshot "xterm -title screencap -e ~/bin/get-screenshot 15 root"
command "browser" firefox-esr
command "mailer" claws-mail
command "graphics editor" gimp
command "music tagger" exfalso

# automatic window groups
autogroup 9 Navigator,Firefox
autogroup 0 xclock,XClock
autogroup 0 xload,XLoad
autogroup 0 xlogo,XLogo

# ignore these windows when tiling/cycling
ignore xclock
ignore xload
ignore xlogo

# remove all default keybindings
unbind-key all

# KEYBINDINGS:
# 4 = mod (windows key)
# S = shift
# C = control
# M = meta (alt)

# mod + enter = new xterm
bind-key 4-Return terminal
# ctrl + alt + L = lock screen (xlock)
bind-key CM-l lock
# mod + backspace = hide window
bind-key 4-BackSpace window-hide

# mod + down arrow = lower window's focus
bind-key 4-Down window-lower
# mod + up arrow = raise window's focus
bind-key 4-Up window-raise
# mod/alt + tab = cycle through current windows
bind-key 4-Tab window-cycle
bind-key M-Tab window-cycle
# mod/alt + shift + tab = same thing in reverse
bind-key 4S-Tab window-rcycle
bind-key MS-Tab window-rcycle
# mod + w = kill window
bind-key 4-w window-delete
# set a window's label
bind-key 4-n window-menu-label

# mod + $N = show only windows for group $N
# (you can think of this as switching to virtual desktop $N)
bind-key 4-1 group-only-1
bind-key 4-2 group-only-2
bind-key 4-3 group-only-3
bind-key 4-4 group-only-4
bind-key 4-5 group-only-5
bind-key 4-6 group-only-6
bind-key 4-7 group-only-7
bind-key 4-8 group-only-8
bind-key 4-9 group-only-9

# mod + shift +$N = move window to group $N
bind-key 4S-1 window-movetogroup-1
bind-key 4S-2 window-movetogroup-2
bind-key 4S-3 window-movetogroup-3
bind-key 4S-4 window-movetogroup-4
bind-key 4S-5 window-movetogroup-5
bind-key 4S-6 window-movetogroup-6
bind-key 4S-7 window-movetogroup-7
bind-key 4S-8 window-movetogroup-8
bind-key 4S-9 window-movetogroup-9

# mod + A = toggle showing windows from all groups
bind-key 4-a group-toggle-all
# mod + G = toggle current window's group membership
bind-key 4-g window-group

# mod + right arrow = cycle through the window groups
# (you can think of this as switching to the next virtual desktop)
bind-key 4-Right group-cycle
# mod + left arrow = same thing in reverse
bind-key 4-Left group-rcycle
# mod + S = stick current window to be visible in all groups
bind-key 4-s window-stick

# mod + F = make current window fullscreen
bind-key 4-f window-fullscreen
# mod + M = maximize current window
bind-key 4-m window-maximize
# mod + equals = maximize window in vertical direction only
bind-key 4-equal window-vmaximize
# mod + shift + equals = maximize window in horizontal direction only
bind-key 4S-equal window-hmaximize

# mod + H,J,K,L = move window left, down, up, right 10 pixels
bind-key 4-h window-move-left-big
bind-key 4-j window-move-down-big
bind-key 4-k window-move-up-big
bind-key 4-l window-move-right-big

# mod + shift + H,J,K,L = resize window left, down, up, right by 10 pixels
bind-key 4S-h window-resize-left-big
bind-key 4S-j window-resize-down-big
bind-key 4S-k window-resize-up-big
bind-key 4S-l window-resize-right-big

# mod + V = tile windows vertically, current window to the left
bind-key 4-v window-vtile
# mod + V = tile windows horizontally, current window to the top
bind-key 4-c window-htile

# mod + / = show popup menu of current windows
bind-key 4-slash menu-window
# mod + d = show popup menu to run a command (configured below)
bind-key 4-d menu-cmd
# mod + ? = show popup menu to run an arbitrary command
bind-key 4-question menu-exec
# mod + . = show popup menu of known ssh hosts to connect to
bind-key 4-period menu-ssh

# mod + shift + R = restart cwm, reloading configuration
bind-key 4S-r restart
# mod + shift + E = log out
bind-key 4S-e quit

# unbind default mouse actions
unbind-mouse M-1
unbind-mouse CM-1
unbind-mouse M-2
unbind-mouse M-3
unbind-mouse CMS-3

# mod + left click drag = move window
bind-mouse 4-1 window-move
# mod + right click drag = resize window
bind-mouse 4-3 window-resize
# mod + middle click = lower window's focus
bind-mouse 4-2 window-lower
# mod + shift + middle click = hide window
bind-mouse 4S-2 window-hide
