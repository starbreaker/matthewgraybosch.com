; .emacs
; Copyright 2021 Matthew Graybosch <contact@matthewgraybosch.com>

; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions
; are met:

; 1. Redistributions of source code must retain the above copyright
; notice, this list of conditions and the following disclaimer.

; 2. Redistributions in binary form must reproduce the above
; copyright notice, this list of conditions and the following
; disclaimer in the documentation and/or other materials provided
; with the distribution.

; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
; FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
; OF THE POSSIBILITY OF SUCH DAMAGE.

;;; Code:
(setq gc-cons-threshold 50000000)
(setq large-file-warning-threshold 100000000)

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

(column-number-mode t)
(size-indication-mode t)

(setq inhibit-splash-screen t)
(setq c-basic-offset 4)     ; indents 4 chars
(setq tab-width 4)          ; and 4 char wide for TAB
(setq indent-tabs-mode nil) ; And force use of spaces

(use-package oceanic-theme
  :ensure t					  
  :config					  
  (load-theme 'oceanic t))

(use-package ledger-mode
  :ensure t)

;; Emacs Zettelkasten
(use-package deft
  :ensure t
  :custom
  (deft-extensions '("org" "md" "txt"))
  (deft-directory "~/notes")
  (deft-use-filename-as-title t))

(use-package zetteldeft
  :ensure t
  :after deft
  :config (zetteldeft-set-classic-keybindings))

(setq user-full-name "Matthew Graybosch"
      user-mail-address "contact@matthewgraybosch.com")

(use-package unfill
  :ensure t
  :config
  (global-set-key (kbd "<f8>") 'toggle-fill-unfill))

(use-package async
  :ensure t
  :init (dired-async-mode 1))

(use-package elpher
  :ensure t)

;; stuff for eww (emacs web browser)
(set browse-url-browser-function 'eww-browse-url)
(setq url-cookie-trusted-urls '("https://midnight.pub")
      url-cookie-untrusted-urls '(".*"))

(advice-add 'eww-browse-url :around 'asc:eww-browse-url)

(defun asc:eww-browse-url (original url &optional new-window)
  "Handle gemini links."
  (cond ((string-match-p "\\`\\(gemini\\|gopher\\)://" url)
		 (require 'elpher)
		 (elpher-go url))
		(t (funcall original url new-window))))

(use-package gemini-mode
  :ensure t)

(use-package web-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.njk\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode)))

(use-package emmet-mode
  :ensure t
  :init
  (add-hook 'sgml-mode-hook 'emmet-mode)
  (add-hook 'html-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook  'emmet-mode)
  (add-hook 'm4-mode-hook  'emmet-mode))

(use-package markdown-mode
  :ensure t
  :mode (("README\\.md\\'" . gfm-mode)
		 ("\\.md\\'" . markdown-mode)
         ("\\.txt\\'" . markdown-mode)
		 ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "/usr/local/cabal/bin/pandoc"))

(setq frame-title-format
      '((:eval (if (buffer-file-name)
				   (abbreviate-file-name (buffer-file-name))
				 "%b"))))

(setq scroll-margin 0
      scroll-conservatively 100000
      scroll-preserve-screen-position 1)

(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(fset 'yes-or-no-p 'y-or-n-p)

(global-auto-revert-mode t)

(setq-default tab-width 4
			  indent-tabs-mode t)

(global-set-key (kbd "C-x k") 'kill-this-buffer)

;; printing fun
(setq lpr-command "/usr/local/bin/gtklp")
(setq ps-lpr-command "/usr/local/bin/gtklp")

;; fun with dired
;; OpenBSD ls isn't compatible with Emacs dired
;; I fixed this with "doas pkg_add -iv coreutils"
(setq insert-directory-program "/usr/local/bin/gls")
(eval-after-load "dired"
  '(require 'dired-x))

(setq dired-recursive-deletes 'always
      dired-recursive-copies 'always
      dired-deletion-confirmer 'y-or-n-p
      dired-clean-up-buffers-too nil
      delete-by-moving-to-trash t
      trash-directory "~/.Trash/emacs"
      dired-dwim-target t
      dired-guess-shell-alist-user
      '(("\\.pdf\\'" "evince")
		("\\.jpg\\'" "comix")
		("\\.png\\'" "comix")
		("\\.gif\\'" "comix"))
      dired-listing-switches "-alv")

(add-hook 'dired-mode-hook
	  (lambda ()
	    (local-set-key "E" 'emms-play-dired)
	    (local-set-key "A" 'emms-add-dired)
	    (local-set-key (kbd "C-c w") 'wdired-change-to-wdired-mode)))

(use-package smartparens
  :ensure t
  :config
  (progn
    (require 'smartparens-config)
    (show-paren-mode t)))

(use-package expand-region
  :ensure t
  :bind ("M-m" . er/expand-region))

(use-package crux
  :ensure t
  :bind
  ("C-k" . crux-smart-kill-line)
  ("C-c n" . crux-cleanup-buffer-or-region)
  ("C-c f" . crux-recentf-find-file)
  ("C-a" . crux-move-beginning-of-line))

(use-package which-key
  :ensure t
  :config
  (which-key-mode +1))

(use-package avy
  :ensure t
  :bind
  ("C-=" . avy-goto-char)
  :config
  (setq avy-background t))

(use-package company
  :ensure t)

(use-package helm
  :ensure t
  :defer 2
  :bind
  ("M-x" . helm-M-x)
  ("C-x C-f" . helm-find-files)
  ("M-y" . helm-show-kill-ring)
  ("C-x b" . helm-mini)
  :config
  (require 'helm-config)
  (helm-mode 1)
  (setq helm-split-window-inside-p t
	helm-move-to-line-cycle-in-source t)
  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)
  (helm-autoresize-mode 1)
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
  (define-key helm-map (kbd "C-z") 'helm-select-action)
  )

(use-package helm-emmet
  :ensure t)

(use-package php-mode
  :ensure t)

(use-package company-php
  :ensure t
  :config
  (add-hook 'php-mode-hook
          '(lambda ()
            (require 'company-php)
            (company-mode t)
            (add-to-list 'company-backends 'company-ac-php-backend))))

;; When two buffers are open with the same name, this makes it easier to tell them apart.
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Org Mode TODO settings
(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "DRAFT(n)" "EDITED DRAFT(e)" "FINAL DRAFT(f)"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
	      ("DRAFT" :foreground "blue" :weight bold)
	      ("EDITED DRAFT" :foreground "gold" :weight bold)
	      ("FINAL DRAFT" :foreground "forest green" :weight bold))))

(global-visual-line-mode t)

(eval-after-load "org"
  '(require 'ox-md nil t))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-safe-themes
   '("06f0b439b62164c6f8f84fdda32b62fb50b6d00e8b01c2208e55543a6337433a" default))
 '(deft-directory "~/notes")
 '(deft-extensions '("org" "md" "txt"))
 '(deft-use-filename-as-title t)
 '(elpher-ipv4-always t)
 '(indicate-buffer-boundaries '((t . right) (top . left)))
 '(indicate-empty-lines t)
 '(package-selected-packages
   '(melancholy-theme gruvbox-theme company-php php-mode ledger-mode oceanic-theme base16-theme zetteldeft deft web-mode wc-mode poet-theme material-theme emms which-key use-package unfill smartparens markdown-mode helm-projectile helm-emmet gemini-mode flycheck expand-region elpher diminish crux company color-theme-sanityinc-tomorrow avy))
 '(save-place-mode t)
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(tool-bar-mode nil))
(put 'upcase-region 'disabled nil)
(setq visible-bell t
      completion-ignore-case t
      read-buffer-completion-ignore-case t)
(setq sentence-end-double-space nil)

;; run emacs in daemon mode.
;; (server-start)
(put 'downcase-region 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
