#!/bin/sh -e

URL="$1"
DEST="$2"
ID=$(echo $URL | cut -d = -f 2)
JPG="${DEST}/${ID}.jpg"
THUMB_URL="http://i3.ytimg.com/vi/${ID}/hqdefault.jpg"

curl ${THUMB_URL} --output ${JPG}

echo "
<p>
  <a href=\"https://www.youtube.com/watch?v=${ID}\"
     title=\"click to watch in another tab\"
     target=\"_blank\"
     rel=\"noopener nofollow\">
     <picture>
       <source srcset=\"/images/thumb/${ID}.webp\"
               media=\"(min-width: 240px)\">
       <img src=\"/images/thumb/${ID}.jpg\" alt=\"YouTube thumbnail\" />
    </picture>
  </a>
</p>" | xclip

echo "Downloaded ${JPG}. Middle-click to paste thumbnail markup."
