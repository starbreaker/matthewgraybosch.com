# matthewgraybosch.com

This is my personal website, built with:

* [GNU Emacs][1]
* HTML5
* ~~CSS3~~
* ~~JavaScript~~
* ~~RSS~~
* a BSD makefile
* [HTML/XML Utils][2]
* [ImageMagick][3]
* [rsync][4]

It's tested on the following web browsers:

* [lynx][5]
* [Mozilla Firefox ESR][6]

It's eligible for membership in the following:

* [nojs.club][7]
* [nocss.club][8]
* [xhtml.club][9]
* [250kb.club][10]
* [512kb.club][11]
* [1mb.club][12]

Why am I building a website without CSS, JavaScript, or even a RSS feed?

Because all I want is a [motherfucking website][13].

[1]: https://www.gnu.org/software/emacs/
[2]: https://www.w3.org/Tools/HTML-XML-utils/
[3]: https://imagemagick.org/
[4]: https://rsync.samba.org/
[5]: https://invisible-island.net/lynx/
[6]: https://www.mozilla.org/en-US/firefox/all/#product-desktop-esr
[7]: https://nojs.club/
[8]: https://nocss.club/
[9]: https://xhtml.club/
[10]: https://250kb.club/
[11]: https://512kb.club/
[12]: https://1mb.club/
[13]: http://motherfuckingwebsite.com/
